package com.hyperlogy.smartekyc.EkycAction;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.hyperlogy.smartekyc.EkycAction.EkycMyCall.OnMyBtnCancleDialog;
import com.hyperlogy.smartekyc.EkycAction.EkycMyCall.OnMyBtnDialog;

public class MessageHelper {
    public  static  String messCode500="Có lỗi xảy ra trong quá trình xác thực chứng minh. Vui lòng thử lại";
    public static void openDialog(Context context, String title, String message, String btnBoQua, String btnThuLai, OnMyBtnDialog myCLickCancle) {
        AlertDialog.Builder b = new AlertDialog.Builder(context);
        //
        b.setCancelable(false);
        b.setTitle(title);
        b.setMessage(message);
        b.setPositiveButton(btnBoQua, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                myCLickCancle.onCancle(true);
            }
        });
        b.setNegativeButton(btnThuLai, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                myCLickCancle.OnMyClickTryAgain(true);
            }
        });
        b.show();
    }
    public static void openDialogCancle(Context context, String title, String message, String btnBoQua, OnMyBtnCancleDialog myCLickCancle) {
        AlertDialog.Builder b = new AlertDialog.Builder(context);
        //
        b.setCancelable(false);
        b.setTitle(title);
        b.setMessage(message);
        b.setPositiveButton(btnBoQua, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                myCLickCancle.onCancle(true);
            }
        });
        b.show();
    }
}
