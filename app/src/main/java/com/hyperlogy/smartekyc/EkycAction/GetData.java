package com.hyperlogy.smartekyc.EkycAction;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.hyperlogy.smartekyc.EkycModel.ItemFormData;

import java.util.ArrayList;
import java.util.List;

public class GetData {
    public static void saveUserName_Password(Context context, String username, String password){
        SharedPreferences sharedPreferences =context.getSharedPreferences("data_login", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("userName", username);
        editor.putString("passWord", password);
        editor.commit();
        Log.d("user_pass_login", username+"/"+password);
    }
    public static void saveToken(Context context, String token){
        SharedPreferences sharedPreferences = context.getSharedPreferences("my_token", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Token", token);
        editor.commit();
    }
    public static String getToken(Context context) {
        String token="";
        SharedPreferences sharedPreferences = context.getSharedPreferences("my_token", Context.MODE_PRIVATE);
        if (sharedPreferences != null) {
            token = sharedPreferences.getString("Token", "");
            Log.d("MyToken", token);
        }
        return token;
    }
    public static void saveGuidId(Context context, String guidId){
        SharedPreferences sharedPreferences = context.getSharedPreferences("my_guidID", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("guidID", guidId);
        editor.commit();
    }
    public static String getGuiId(Context context) {
        String guidID="";
        SharedPreferences sharedPreferences = context.getSharedPreferences("my_guidID", Context.MODE_PRIVATE);
        if (sharedPreferences != null) {
           guidID  = sharedPreferences.getString("guidID", "");
            Log.d("guiID:", guidID);
        }
        return guidID;
    }
    public static void saveNumberCard(Context context, String numberCard){
        SharedPreferences sharedPreferences = context.getSharedPreferences("my_numberCard", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("numberCard", numberCard);
        editor.commit();
    }
    public static String getNumberCard(Context context) {
        String numberCard="";
        SharedPreferences sharedPreferences = context.getSharedPreferences("my_numberCard", Context.MODE_PRIVATE);
        if (sharedPreferences != null) {
            numberCard  = sharedPreferences.getString("numberCard", "");
            Log.d("numberCard:", numberCard);
        }
        return numberCard;
    }
    public static void saveImageFace(Context context, List<ItemFormData> list) {
        //---------------------------luu anh khuon mat ------------------------
        ItemFormData itemFormData1= (ItemFormData) list.get(0);
        ItemFormData itemFormData2= (ItemFormData) list.get(1);
        ItemFormData itemFormData3= (ItemFormData) list.get(2);
        SharedPreferences sharedPreferencesFace = context.getSharedPreferences("fileImageFace", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferencesFace.edit();
        editor.putString("nameface1",itemFormData1.getmFile().toString() );
        editor.putString("nameface2",itemFormData2.getmFile().toString() );
        editor.putString("nameface3",itemFormData3.getmFile().toString() );
        editor.commit();
    }
    public static List<Object>  saveObjectListLiveness(Context context,List<ItemFormData> list){
        String guidID=getGuiId(context);
        String userName= getNumberCard(context);
        List<Object> objectList= new ArrayList<>();//3 anh khuon mat va guidid
        objectList.add(list.get(0));
        objectList.add(list.get(1));
        objectList.add(list.get(2));//
        objectList.add(guidID);
        objectList.add(userName);//numberCard
        return objectList;
    }
    public static List<Object>  saveObjectListOCR(Context context,List<ItemFormData> list){
        List<Object> objectList= new ArrayList<>();//2 anh cmt
        objectList.add(list.get(0));
        objectList.add(list.get(1));
        return objectList;
    }
}
