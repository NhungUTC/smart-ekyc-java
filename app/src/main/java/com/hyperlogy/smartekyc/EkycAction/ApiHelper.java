package com.hyperlogy.smartekyc.EkycAction;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.hyperlogy.smartekyc.EkycAction.EkycMyCall.OnMyBtnDialog;
import com.hyperlogy.smartekyc.EkycAction.EkycMyCall.OnPostDataSuccess;
import com.hyperlogy.smartekyc.EkycActivity.LoginActivity;
import com.hyperlogy.smartekyc.EkycUntil.EkycCheckConnect.CheckInternet;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ApiHelper {
    private static long TIME_OUT = 60;//mili seconds

    public static void OkHttpGet(Context context, String urlGet, OnPostDataSuccess myCallback) {
        if (CheckInternet.haveNetworkConnection(context)) {
            Log.d("waitCallApi", "waitCallApi");
            ProgressDialog dialog;
            dialog = new ProgressDialog(context);
            dialog.setMessage("loading...");
            dialog.show();
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(TIME_OUT, TimeUnit.SECONDS) // connect timeout
//                    .writeTimeout(1, TimeUnit.MINUTES) // write timeout
//                    .readTimeout(1, TimeUnit.MINUTES) // read timeout
                    .build();
            Request request = new Request.Builder()
                    .url(urlGet)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            dialog.cancel();
                           // CheckInternet.dialogGetToken(context, "Thông báo", "Time out", "Làm lại");
                            //time out
                            MessageHelper.openDialog(context, "Thông báo", "Time out", "Bỏ qua", "Thử lại", new OnMyBtnDialog() {
                                @Override
                                public void onCancle(Boolean isCancle) {
                                    if(isCancle==true){
                                        Intent intent = new Intent(context, LoginActivity.class);
                                        context.startActivity(intent);
                                    }
                                }

                                @Override
                                public void OnMyClickTryAgain(Boolean isTryAgain) {
                                    if(isTryAgain==true){
                                        OkHttpGet(context, urlGet,myCallback);
                                    }
                                }
                            });
                            Log.d("error_getToken", call.request().body().toString());
                            Log.d("error_getToken", e.toString());
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    dialog.cancel();
                    String bodyRes = response.body().string();
                    Log.d("resPost", bodyRes);
                    myCallback.onPostSuccess(bodyRes);
                }
            });
        } else {
          //  CheckInternet.dialogGetToken(context, "Thông báo", "Bạn hãy kiểm tra lại kết nối Internet", "OK");
            //check internet
            MessageHelper.openDialog(context, "Thông báo", "Vui lòng kiểm tra kết nối mạng", "Bỏ qua", "Thử lại", new OnMyBtnDialog() {
                @Override
                public void onCancle(Boolean isCancle) {
                    if(isCancle==true){
                        Intent intent = new Intent(context, LoginActivity.class);
                        context.startActivity(intent);
                    }
                }

                @Override
                public void OnMyClickTryAgain(Boolean isTryAgain) {
                    if(isTryAgain==true){
                        OkHttpGet(context, urlGet,myCallback);
                    }
                }
            });
        }

    }

    public static void OkHttpPost(Context context, String urlPost, RequestBody requestBody, Boolean isToken, OnPostDataSuccess myCallBack) {
        if (CheckInternet.haveNetworkConnection(context)) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(TIME_OUT, TimeUnit.SECONDS) // connect timeout
//                    .writeTimeout(10, TimeUnit.SECONDS) // write timeout
//                    .readTimeout(10, TimeUnit.SECONDS) // read timeout
                    .build();
            Request request = null;
            if (isToken == true) {
                String token = GetData.getToken(context);
                request = new Request.Builder()
                        .url(urlPost)
                        .post(requestBody)
                        .header("Authorization", "Bearer " + token)
                        .addHeader("content-type", "application/json; charset=utf-8")
                        .build();
            } else {
                request = new Request.Builder()
                        .url(urlPost)
                        .post(requestBody)
                        .addHeader("content-type", "application/json; charset=utf-8")
                        .build();
            }

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("error_sendPictureFace", call.request().body().toString());
                            Log.d("error_sendPictureFace", e.toString());
                        //    CheckInternet.checkTimeoutPost(context, "Thông báo", "Time out", "Đăng nhập lại");
                            //check time out post
                            MessageHelper.openDialog(context, "Thông báo", " Time out", "Bỏ qua", "Thử lại", new OnMyBtnDialog() {
                                @Override
                                public void onCancle(Boolean isCancle) {
                                    if(isCancle==true){
                                        Intent intent = new Intent(context, LoginActivity.class);
                                        context.startActivity(intent);
                                    }
                                }

                                @Override
                                public void OnMyClickTryAgain(Boolean isTryAgain) {
                                    if(isTryAgain==true){
                                        OkHttpPost(context, urlPost, requestBody, isToken, myCallBack);
                                    }
                                }
                            });
                        }
                    });

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    int codecheck = response.code();
                    Log.d("codecheck", codecheck + "");
                    if (codecheck == 200) {
                        if (response.isSuccessful()) {
                            String stringRes = response.body().string();
                            myCallBack.onPostSuccess(stringRes);
                            Log.d("sendPictureFace_succ:", stringRes);
                            Log.d("sendPictureFace", "running");
                        }
                    } else {
                        //status code khac 200
                        CheckInternet.checkTimeoutPost(context, "Thông báo", "Yêu cầu thất bại, mời bạn gửi lại", "Đang nhập lại");
                    }
                }
            });
        } else {
            //check connect internet false
            MessageHelper.openDialog(context, "Thông báo", "Vui lòng kiểm tra kết nối mạng", "Bỏ qua", "Thử lại", new OnMyBtnDialog() {
                @Override
                public void onCancle(Boolean isCancle) {
                    if(isCancle==true){
                        Intent intent = new Intent(context, LoginActivity.class);
                        context.startActivity(intent);
                    }
                }

                @Override
                public void OnMyClickTryAgain(Boolean isTryAgain) {
                    if(isTryAgain==true){
                        OkHttpPost(context, urlPost, requestBody, isToken, myCallBack);
                    }
                }
            });
        }

    }

}
