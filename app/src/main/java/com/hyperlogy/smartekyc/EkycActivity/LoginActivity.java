package com.hyperlogy.smartekyc.EkycActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.hyperlogy.smartekyc.EkycUntil.EkycApi.Api;
import com.hyperlogy.smartekyc.R;
import com.hyperlogy.smartekyc.EkycAction.ApiHelper;
import com.hyperlogy.smartekyc.EkycAction.GetData;
import com.hyperlogy.smartekyc.EkycAction.EkycMyCall.OnPostDataSuccess;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnGetToken;
    ImageView imgLogo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mapping();
      //  setCompany();
        btnGetToken.setOnClickListener(this);
        btnGetToken.performClick();
    }
    private boolean checkInputEditext(EditText edt) {
        if (edt.getText().toString().trim().length() > 0) {
            return true;
        } else {
            edt.setError("Bạn hãy nhập dữ liệu!");
        }
        return false;
    }

    private void mapping() {
        btnGetToken = findViewById(R.id.btn_get_token);
        imgLogo = findViewById(R.id.img_logo);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getTokenData() {
        String urlGetToken = Api.getUrlGetToken();
        ApiHelper.OkHttpGet(LoginActivity.this, urlGetToken, new OnPostDataSuccess() {
            @Override
            public void onPostSuccess(String response) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("getToken_response", response);
                        try {
                            JSONObject objResponse = new JSONObject(response);
                            String token = objResponse.getString("Token");
                            Log.d("MyToken", token);
                            GetData.saveToken(getApplicationContext(), token);
                            Intent intent = new Intent(LoginActivity.this, OcrActivity.class);
                            startActivity(intent);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_get_token:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    getTokenData();
                }
                break;
        }
    }
}