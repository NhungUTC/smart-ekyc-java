
package com.hyperlogy.smartekyc.EkycActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.extensions.HdrImageCaptureExtender;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.hyperlogy.smartekyc.EkycAction.EkycMyCall.OnMyBtnCancleDialog;
import com.hyperlogy.smartekyc.EkycUntil.EkycApi.Api;
import com.hyperlogy.smartekyc.R;
import com.hyperlogy.smartekyc.EkycAction.ApiHelper;
import com.hyperlogy.smartekyc.EkycAction.GetData;
import com.hyperlogy.smartekyc.EkycAction.MessageHelper;
import com.hyperlogy.smartekyc.EkycAction.EkycMyCall.OnMyBtnDialog;
import com.hyperlogy.smartekyc.EkycAction.EkycMyCall.OnPostDataSuccess;
import com.hyperlogy.smartekyc.EkycModel.ItemFormData;
import com.hyperlogy.smartekyc.EkycModel.UserInfor;
import com.bumptech.glide.Glide;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import okhttp3.RequestBody;

public class OcrActivity extends AppCompatActivity implements View.OnClickListener {
    private final String[] REQUIRED_PERMISSIONS = new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"};
    public Executor executor = Executors.newSingleThreadExecutor();
    public static ImageCapture imageCapture;
    PreviewView mPreviewView;
    boolean font = true;//check taking front card
    int lensFacing = CameraSelector.LENS_FACING_BACK;//camera sau
    CameraSelector cameraSelector;
    ImageAnalysis imageAnalysis;
    Preview preview;
    Button btnTakeFrontcard, btnTakeBackcard;
    Button btnOK1, btnLamLai1;
    Button btnOk2, btnLamLai2;

    Button btnSuccessOCR;
    Boolean successOCR = false;
    LinearLayout viewTakeFrontCard, viewTakeBackCard;//view chứa button chụp mặt trước-sau, button ok, button làm lại
    TextView txtStatusTakePicture;
    ImageView imgSq;
    public static ProgressBar processBarOCR;
    ItemFormData itemCardFront;
    ItemFormData itemCardBack;
    ImageView imgTakeCard;
    List<Object> listObjRootOCR = new ArrayList<>();
    private final int REQUEST_CODE_PERMISSIONS = 1001;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ocr);
        mapping();
        setOnClickListener();
        if (allPermissionsGranted()) {
            startCamera(); //start camera if permission has been granted by user
        } else {
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }
        setVisibleButton();
    }
    private void setVisibleButton() {
        btnTakeFrontcard.setVisibility(View.VISIBLE);
        btnLamLai1.setVisibility(View.GONE);
        btnOK1.setVisibility(View.GONE);
        txtStatusTakePicture.setText("Chụp mặt trước");
    }
    private void setOnClickListener() {
        if (btnTakeFrontcard != null) {
            btnTakeFrontcard.setOnClickListener(this);
        }
        btnLamLai1.setOnClickListener(this);
        btnOK1.setOnClickListener(this);
        btnTakeBackcard.setOnClickListener(this);
        btnLamLai2.setOnClickListener(this);
        btnOk2.setOnClickListener(this);
        btnTakeBackcard.setOnClickListener(this);
        btnSuccessOCR.setOnClickListener(this);
    }

    private void mapping() {
        mPreviewView = findViewById(R.id.previewView);
        txtStatusTakePicture = findViewById(R.id.txt_status_take_picture);
        btnTakeFrontcard = findViewById(R.id.btn_take_front_card);
        btnOK1 = findViewById(R.id.btn_ok_1);
        btnLamLai1 = findViewById(R.id.btn_Lam_lai_1);
        btnTakeBackcard = findViewById(R.id.btn_take_back_card);
        btnOk2 = findViewById(R.id.btn_ok_2);
        btnLamLai2 = findViewById(R.id.btn_Lam_lai_2);
        viewTakeBackCard = findViewById(R.id.view_take_back_card);
        viewTakeFrontCard = findViewById(R.id.view_take_front_card);
        imgSq = findViewById(R.id.img_sq);
        processBarOCR = findViewById(R.id.process_bar_ocr);
        imgTakeCard = findViewById(R.id.img_take_card);
        btnSuccessOCR = findViewById(R.id.btn_success_ocr);
    }

    private void startCamera() {
        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        cameraProviderFuture.addListener(new Runnable() {
            @Override
            public void run() {
                try {
                    ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                    bindPreview(cameraProvider, lensFacing);
                } catch (ExecutionException | InterruptedException e) {
                    // No errors need to be handled for this Future.
                    // This should never be reached.
                }
            }
        }, ContextCompat.getMainExecutor(this));
    }

    void bindPreview(@NonNull ProcessCameraProvider cameraProvider, int myLensFacing) {
        preview = new Preview.Builder()
                .build();
        cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(myLensFacing)
                .build();
        imageAnalysis = new ImageAnalysis.Builder()
                .build();
        ImageCapture.Builder builder = new ImageCapture.Builder().setTargetResolution(new Size(480, 720));
        //Vendor-Extensions (The CameraX extensions dependency in build.gradle)
        HdrImageCaptureExtender hdrImageCaptureExtender = HdrImageCaptureExtender.create(builder);
        // Query if extension is available (optional).
        if (hdrImageCaptureExtender.isExtensionAvailable(cameraSelector)) {
            // Enable the extension if available.
            hdrImageCaptureExtender.enableExtension(cameraSelector);
        }
        imageCapture = builder
                .setTargetRotation(this.getWindowManager().getDefaultDisplay().getRotation())
                .build();
        preview.setSurfaceProvider(mPreviewView.createSurfaceProvider());

        Camera camera = cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageAnalysis, imageCapture);

    }

    private boolean allPermissionsGranted() {
        for (String permission : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera();
            } else {
                Toast.makeText(this, "Permissions not granted by the user.", Toast.LENGTH_SHORT).show();
                this.finish();
            }
        }
    }
    //chụp mặt trước sau chứng minh thư
    private void takePicture() {
        if (font == true) {
            btnTakeFrontcard.setText("Đang chụp...");
            btnLamLai1.setVisibility(View.GONE);
            btnOK1.setVisibility(View.GONE);
        } else {
            btnTakeBackcard.setText("Đang chụp...");
            btnLamLai2.setVisibility(View.GONE);
            btnOk2.setVisibility(View.GONE);
        }
        SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                mDateFormat.format(new Date()) + ".jpg");
        ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions.Builder(file).build();
        imageCapture.takePicture(outputFileOptions, executor, new ImageCapture.OnImageSavedCallback() {
            @Override
            public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        SharedPreferences sharedPreferences = getSharedPreferences("fileCard", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        if (font == true) {
                            btnTakeFrontcard.setVisibility(View.GONE);
                            btnTakeFrontcard.setText("Chụp mặt trước");
                            btnLamLai1.setVisibility(View.VISIBLE);
                            btnOK1.setVisibility(View.VISIBLE);
                            File pathFront = file;
                            //---------------------------hien anh mat truoc-----------------
                            imgTakeCard.setVisibility(View.VISIBLE);
                            imageTakeCard(file);
                            //sharedPreferences
                            editor.putString("fileFrontCard", pathFront + "");
                            String nameImg = "fontIdCard" + new Date().getTime() + ".jpg";
                            editor.putString("nameFrontCard", nameImg);
                            String type = "image/jpeg";
                            itemCardFront = new ItemFormData(pathFront, nameImg, type);
                            Log.d("itemCardFront", itemCardFront + "");
                        } else {
                            btnTakeBackcard.setVisibility(View.GONE);
                            btnTakeBackcard.setText("Chụp mặt sau");
                            btnLamLai2.setVisibility(View.VISIBLE);
                            btnOk2.setVisibility(View.VISIBLE);
                            File pathBack = file;
                            //-------------hien anh mat sau-----------------
                            imgTakeCard.setVisibility(View.VISIBLE);
                            imageTakeCard(file);
                            //sharedPreferences
                            editor.putString("fileBackCard", pathBack + "");
                            String nameImg = "backIdCard" + new Date().getTime() + ".jpg";
                            String type = "image/jpeg";
                            itemCardBack = new ItemFormData(pathBack, nameImg, type);
                            Log.d("itemCardback:", itemCardBack + "");
                        }
                        //sharedPreferences
                        editor.commit();
                    }
                });
            }
            @Override
            public void onError(@NonNull ImageCaptureException error) {
                error.printStackTrace();
            }
        });
    }
    private void imageTakeCard(File fileCard) {
        Log.d("fileCard", fileCard.toString());
        if (fileCard.exists()) {
            Glide.with(getApplicationContext()).load(fileCard).into(imgTakeCard);
        } else {
            Toast.makeText(getApplicationContext(), "File not Exist", Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_take_front_card:
                takePicture();
                font = true;
                Log.d("front", "" + font);
                break;
            case R.id.btn_Lam_lai_1:
                btnTakeFrontcard.setVisibility(View.VISIBLE);
                btnLamLai1.setVisibility(View.GONE);
                btnOK1.setVisibility(View.GONE);
                imgTakeCard.setVisibility(View.GONE);
                break;
            case R.id.btn_ok_1:
                viewTakeBackCard.setVisibility(View.VISIBLE);
                btnOk2.setVisibility(View.GONE);
                btnLamLai2.setVisibility(View.GONE);
                viewTakeFrontCard.setVisibility(View.GONE);
                txtStatusTakePicture.setText("Chụp mặt sau");
                btnTakeBackcard.setVisibility(View.VISIBLE);//
                imgTakeCard.setVisibility(View.GONE);
                font=false;
                break;
            case R.id.btn_take_back_card:
                font = false;
                takePicture();
                Log.d("back", "" + font);
                break;
            case R.id.btn_Lam_lai_2:
                btnTakeBackcard.setVisibility(View.VISIBLE);
                btnLamLai2.setVisibility(View.GONE);
                btnOk2.setVisibility(View.GONE);
                imgTakeCard.setVisibility(View.GONE);
                break;
            case R.id.btn_ok_2:
                processBarOCR.setVisibility(View.VISIBLE);// đợi check giấy tờ
                imgTakeCard.setVisibility(View.VISIBLE);
                txtStatusTakePicture.setText("Đang kiểm tra giấy tờ...");
                btnOk2.setVisibility(View.GONE);
                btnLamLai2.setVisibility(View.GONE);
                //tạo form data gồm ảnh mặt trước sau cmt, token, doctype
                createFormData();
                break;
            case R.id.btn_success_ocr:
                imgTakeCard.setVisibility(View.GONE);
                btnSuccessOCR.setVisibility(View.GONE);
                //-------------------OCR-----------------------------
                UserInfor.Root rootOCR = (UserInfor.Root) listObjRootOCR.get(0);
                Log.d("rootOCRmain", rootOCR + "");
                UserInfor.Data _data = rootOCR.getData();
                UserInfor.IdCard idCard = _data.getIdCard();
                Log.d("UserInfor.Data", _data.toString());
                if (successOCR == true) {
                    Intent intent=new Intent(OcrActivity.this, LivenessActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("idcard", idCard);
                    bundle.putParcelable("rootOCR", rootOCR);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    //------------------------------------------------------------------------------------
                   // finish();
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + v.getId());
        }
    }
    // send picture image card
    private void createFormData() {
        List<ItemFormData> listImagecard= new ArrayList<>();
        listImagecard.add(itemCardFront);
        listImagecard.add(itemCardBack);
        Log.d("listCard", listImagecard.toString());
        List<Object> listObjOCR= GetData.saveObjectListOCR(getApplicationContext(),listImagecard);
       // String urlOCR=AppConfig.apiType.getUrlOCR();
        String urlOCR= Api.getUrlOCR();
      //  RequestBody requestBody= AppConfig.apiType.getRequestBodyOCR(listObjOCR);
        RequestBody requestBody= Api.getRequestBodyOCR(listObjOCR);
        ApiHelper.OkHttpPost(OcrActivity.this, urlOCR, requestBody, true, new OnPostDataSuccess() {
            @Override
            public void onPostSuccess(String response) {
                try {
                    JSONObject obj= new JSONObject(response);
                    //--------------------root----------------
                    Gson gsonRoot= new Gson();
                    UserInfor.Root _rootOCR= gsonRoot.fromJson(String.valueOf(obj), UserInfor.Root.class);
                    Log.d("_rootOCR", _rootOCR + "");
                    listObjRootOCR.add(_rootOCR);
                    UserInfor.Data _data = _rootOCR.getData();
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (_rootOCR.getCode() == 200) {
                                Log.d("_dataRootOCR", _data + "");
                                String guidID = _data.getGuidID();
                                GetData.saveGuidId(getApplicationContext(),guidID);
                                String numberCard= _data.getIdCard().getIdentCardNumber();// so cmt
                                GetData.saveNumberCard(OcrActivity.this, numberCard);
                                processBarOCR.setVisibility(View.GONE);
                                successOCR = true;
                                btnSuccessOCR.setText("Kiểm tra khuôn mặt");
                                txtStatusTakePicture.setText("Kiểm tra giấy tờ thành công");
                                btnSuccessOCR.setVisibility(View.VISIBLE);
                            } else {
                                processBarOCR.setVisibility(View.GONE);
                                successOCR = false;
                                btnSuccessOCR.setText("Chụp lại giấy tờ");
                                txtStatusTakePicture.setText("");
//                                imgTakeCard.setVisibility(View.GONE);
                                //   btnSuccessOCR.setVisibility(View.VISIBLE); thay bang dialog
                                //check code >500
                                String message;
                                if(_rootOCR.getCode() == 501|| _rootOCR.getCode() == 502|| _rootOCR.getCode() == 503){
                                   message=MessageHelper.messCode500;
                                }else{
                                  message=_rootOCR.getMessages();
                                }
                                MessageHelper.openDialogCancle(OcrActivity.this, "Thông báo",message, "Làm lại", new OnMyBtnCancleDialog() {
                                    @Override
                                    public void onCancle(Boolean isCancle) {
                                       Intent intent= new Intent(OcrActivity.this, OcrActivity.class);
                                       startActivity(intent);
                                    }
                                });

                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
