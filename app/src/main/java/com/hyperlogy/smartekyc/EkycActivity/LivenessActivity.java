package com.hyperlogy.smartekyc.EkycActivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.extensions.HdrImageCaptureExtender;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;
import com.hyperlogy.smartekyc.EkycAction.ApiHelper;
import com.hyperlogy.smartekyc.EkycAction.EkycMyCall.OnPostDataSuccess;
import com.hyperlogy.smartekyc.EkycAction.GetData;
import com.hyperlogy.smartekyc.EkycModel.ItemFormData;
import com.hyperlogy.smartekyc.EkycModel.ResultFace;
import com.hyperlogy.smartekyc.EkycModel.UserInfor;
import com.hyperlogy.smartekyc.EkycUntil.EkycApi.Api;
import com.hyperlogy.smartekyc.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import okhttp3.RequestBody;

public class LivenessActivity extends AppCompatActivity {
    private Executor executor = Executors.newSingleThreadExecutor();
    private int REQUEST_CODE_PERMISSIONS = 1001;
    private final String[] REQUIRED_PERMISSIONS = new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"};
    PreviewView mPreviewView;
    int lensFacing = CameraSelector.LENS_FACING_FRONT;//camera sau
    CameraSelector cameraSelector;
    ImageAnalysis imageAnalysis;
    Preview preview;
    //  Button btnLiveness;
    TextView txtStatusLiveness;
    TextView txtPbarLiveness;
    ProgressBar progressBarLiveness;
    int i = 0;//bien set Process bar take face
    public static ImageCapture imageCapture;
    private final int WAIT_TIME = 2000;
    int COUNT = 0;
    ArrayList<ItemFormData> listPictureFace = new ArrayList<>();// ảnh chụp khuôn mặt
    File filePicface;// duong dan anh chup khuon mat
    private UserInfor.Root rootOCR;
    private UserInfor.IdCard idCardObjUser = new UserInfor.IdCard();
    private UserInfor.IdCard idCardObjUserSend = new UserInfor.IdCard();
    private UserInfor.IdCard idCardObjUserFromResult = new UserInfor.IdCard();
    public static int REQUEST_START_ACTIVITY_LIVENESS = 1111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liveness);
        mapping();
        if (allPermissionsGranted()) {
            startCamera(); //start camera if permission has been granted by user
        } else {
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }
        changeFrontCamera();
        getDataOCR();
        Log.d("runningtest", "oncreate");
    }

    private void getDataOCR() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            idCardObjUser = (UserInfor.IdCard) bundle.getSerializable("idcard");
            Log.d("UserInforLiveness", idCardObjUser + "");
            rootOCR = bundle.getParcelable("rootOCR");
            Log.d("rootOCR_result", rootOCR + "");
        }
    }

    private void changeFrontCamera() {
        progressBarLiveness.setVisibility(View.VISIBLE);
        txtPbarLiveness.setVisibility(View.VISIBLE);
        txtPbarLiveness.setText("");
        progressBarLiveness.setProgress(10);
        i = progressBarLiveness.getProgress();
        waitTakeFace();
    }

    private void waitTakeFace() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("takeFace", "take face");
                takeFace();
            }
        }, WAIT_TIME);
    }

    //chụp khuôn mặt
    public void takeFace() {
        i += 30;
        progressBarLiveness.setProgress(i);
        txtPbarLiveness.setText(i + "/" + progressBarLiveness.getMax());
        //  Toast.makeText(getApplicationContext(), "Đang chụp...", Toast.LENGTH_SHORT).show();
        SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
        filePicface = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                mDateFormat.format(new Date()) + ".jpg");
        ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions.Builder(filePicface).build();
        imageCapture.takePicture(outputFileOptions, executor, new ImageCapture.OnImageSavedCallback() {
            @Override
            public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        // Toast.makeText(LivenessActivity.this, "Image Saved successfully", Toast.LENGTH_SHORT).show();
                        String nameImg = "facePicture" + new Date().getTime() + ".jpg";
                        String type = "image/jpeg";
                        listPictureFace.add(new ItemFormData(filePicface, nameImg, type));
                        Log.d("listPicface2: ", listPictureFace + "");
                        Log.d("CountInTake", COUNT + "");
                        COUNT++;
                        if (COUNT < 3) {
                            waitTakeFace();
                        }
                        if (COUNT == 3) {
                            //Finish
                            txtStatusLiveness.setText("");
                            txtPbarLiveness.setText("Đang xác thực...");
                            Log.d("Finish_list_takePicture", listPictureFace + "");
                            //save image picture face
                            GetData.saveImageFace(getApplicationContext(), listPictureFace);
                            List<Object> objectList = GetData.saveObjectListLiveness(getApplicationContext(), listPictureFace);
                            RequestBody requestBody = Api.getRequestBodyVerify(objectList);
                            ApiHelper.OkHttpPost(LivenessActivity.this, Api.getUrlVerifyFace(), requestBody, true, new OnPostDataSuccess() {
                                @Override
                                public void onPostSuccess(String response) {
                                    Log.e("sendPictureFace_succ:", response);
                                    Log.d("sendPictureFace", "running");
                                    try {
                                        JSONObject obj = new JSONObject(response);
                                        Gson gsonLivess = new Gson();
                                        ResultFace.RootLivess rootLivess = gsonLivess.fromJson(String.valueOf(obj), ResultFace.RootLivess.class);
                                        Log.d("rootLivess_", rootLivess + "");

                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                Intent intent = new Intent(LivenessActivity.this, ResultActivity.class);
                                                Bundle bundle = new Bundle();
                                                if (idCardObjUser != null) {
                                                    idCardObjUserSend = idCardObjUser;
                                                } else if (idCardObjUserFromResult != null) {
                                                    idCardObjUserSend = idCardObjUserFromResult;
                                                } else {
                                                    Toast.makeText(LivenessActivity.this, "data  user null", Toast.LENGTH_LONG);
                                                }
                                                bundle.putSerializable("idcard", idCardObjUserSend);
                                                bundle.putParcelable("rootOCR", rootOCR);
                                                // bundle.putSerializable("dataLiveness", rootLivess.getData());
                                                bundle.putSerializable("rootLiveness", rootLivess);
                                                Log.d("dataLiveness", rootLivess.getData() + "");
                                                intent.putExtras(bundle);
                                                startActivityForResult(intent, REQUEST_START_ACTIVITY_LIVENESS);
                                            }
                                        });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            //set count =0 va xoa rong list anh chup khuon mat
                            COUNT = 0;
                            Log.d("count_end", COUNT + "");
                            progressBarLiveness.setVisibility(View.GONE);
                            progressBarLiveness.setProgress(10);
                            listPictureFace.clear();
                            Log.d("Remove_list_takePicture", listPictureFace + "");
                        }
                    }
                });
            }

            @Override
            public void onError(@NonNull ImageCaptureException error) {
                error.printStackTrace();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_START_ACTIVITY_LIVENESS) {
            if (requestCode == ResultActivity.RESULT_CODE_TAKE_FACE_AGAIN) {
                idCardObjUserFromResult = (UserInfor.IdCard) data.getSerializableExtra("result");
                Log.d("userinfor4", idCardObjUserFromResult.toString());
                txtStatusLiveness.setText("Đặt mặt đối diện với camera");
                Log.d("runningtest", "onActivityResult");
                //    changeFrontCamera();
            }
        }
    }

    private void mapping() {
        mPreviewView = findViewById(R.id.previewViewLiveness);
        txtStatusLiveness = findViewById(R.id.txt_status_liveness);
        txtPbarLiveness = findViewById(R.id.txt_info_pbar_livess);
        progressBarLiveness = findViewById(R.id.pBar_liveness);
    }

    private void startCamera() {
        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        cameraProviderFuture.addListener(new Runnable() {
            @Override
            public void run() {
                try {
                    ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                    bindPreview(cameraProvider, lensFacing);
                } catch (ExecutionException | InterruptedException e) {
                    // No errors need to be handled for this Future.
                    // This should never be reached.
                }
            }
        }, ContextCompat.getMainExecutor(this));
    }

    void bindPreview(@NonNull ProcessCameraProvider cameraProvider, int myLensFacing) {
        preview = new Preview.Builder()
                .build();
        cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(myLensFacing)
                .build();
        imageAnalysis = new ImageAnalysis.Builder()
                .build();
        ImageCapture.Builder builder = new ImageCapture.Builder().setTargetResolution(new Size(480, 720));
        //Vendor-Extensions (The CameraX extensions dependency in build.gradle)
        HdrImageCaptureExtender hdrImageCaptureExtender = HdrImageCaptureExtender.create(builder);
        // Query if extension is available (optional).
        if (hdrImageCaptureExtender.isExtensionAvailable(cameraSelector)) {
            // Enable the extension if available.
            hdrImageCaptureExtender.enableExtension(cameraSelector);
        }
        imageCapture = builder
                .setTargetRotation(this.getWindowManager().getDefaultDisplay().getRotation())
                .build();
        preview.setSurfaceProvider(mPreviewView.createSurfaceProvider());

        Camera camera = cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageAnalysis, imageCapture);

    }

    private boolean allPermissionsGranted() {
        for (String permission : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("runningtest", "onRestart");
        changeFrontCamera();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("runningtest", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("runningtest", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("runningtest", "onStop");
    }
}