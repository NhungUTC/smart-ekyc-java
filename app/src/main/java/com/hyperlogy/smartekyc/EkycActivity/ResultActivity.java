package com.hyperlogy.smartekyc.EkycActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.hyperlogy.smartekyc.EkycModel.ResultFace;
import com.hyperlogy.smartekyc.EkycModel.UserInfor;
import com.hyperlogy.smartekyc.R;

import java.io.File;

public class ResultActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText txtCardType;
    private EditText txtIdCard;
    private EditText txtFullName;
    private EditText txtDateOfBirth;
    private EditText txtHometown;//que quan
    private EditText txtAddressResidence;// dia chi thuong tru
    private EditText txtGender;
    private EditText txtIdentCardIssueDate;//ngay cap
    private EditText txtExpireDate;//co gia tri den
    private EditText txtNation_ethnic;//quoc tich/ dan toc
    private ImageView imgFrontCard;
    private ImageView imgBackCard;
    private UserInfor.Root rootOCR;
    private UserInfor.IdCard idCardObjUser = new UserInfor.IdCard();
    private Button btnXacNhanCmt;
    private TextView txtLiveness;
    ResultFace.DataLivess dataLivess = new ResultFace.DataLivess();
    ResultFace.RootLivess rootLivess = new ResultFace.RootLivess();
    private ImageView imgFace;
    public static final int RESULT_CODE_TAKE_FACE_AGAIN = 2222;// code=200, matchingResult=false -> Liveness
    LinearLayout viewInfor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        mapping();
        getInforUser();
        getFilePathCardFromOCR();
        getFaceSuccess();
        btnXacNhanCmt.setOnClickListener(this);
        closeKeyBoard();
    }

    private void closeKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void getFaceSuccess() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            rootLivess = (ResultFace.RootLivess) bundle.getSerializable("rootLiveness");
            // dataLivess= (ResultFace.DataLivess) bundle.getSerializable("dataLiveness");
            //  Log.d("datalv_result", dataLivess+"");
            Log.d("datalv_result", rootLivess + "");
            if (rootLivess.getCode() == 200) {
                txtLiveness.setText("So khớp khuôn mặt thành công");
                btnXacNhanCmt.setText("Đăng ký thành công");
                txtLiveness.setTextColor(Color.parseColor("#4CAF50"));//xanh
            } else if (rootLivess.getCode() == 500 || rootLivess.getCode() == 501 || rootLivess.getCode() == 502) {
                txtLiveness.setText(rootLivess.getMessages());
                btnXacNhanCmt.setText("Quay về trang chủ");
                txtLiveness.setTextColor(Color.parseColor("#F44336"));//do
            } else {
                txtLiveness.setText(rootLivess.getMessages());
                btnXacNhanCmt.setText("Chụp lại khuôn mặt");
                txtLiveness.setTextColor(Color.parseColor("#F44336"));//do
            }
        }
    }

    private void getInforUser() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            idCardObjUser = (UserInfor.IdCard) bundle.getSerializable("idcard");
            Log.d("idcardResultTest", idCardObjUser + "");
            rootOCR = bundle.getParcelable("rootOCR");
            Log.d("rootOCR_result", rootOCR + "");
            txtCardType.setText(idCardObjUser.getIdentCardType());
            txtIdCard.setText(idCardObjUser.getIdentCardNumber());
            txtFullName.setText(idCardObjUser.getIdentCardName());
            txtDateOfBirth.setText(idCardObjUser.getIdentCardBirthDate());
            txtHometown.setText(idCardObjUser.getIdentCardCountry());
            txtAddressResidence.setText(idCardObjUser.getIdentCardAdrResidence());
            txtGender.setText(idCardObjUser.getIdentCardGender());
            txtIdentCardIssueDate.setText(idCardObjUser.getIdentCardIssueDate());
            txtExpireDate.setText(idCardObjUser.getIdentCardExpireDate());
            if (idCardObjUser.getIdentCardNation() != "") {
                txtNation_ethnic.setText(idCardObjUser.getIdentCardNation());
            } else if (idCardObjUser.getIdentCardEthnic() != "") {
                txtNation_ethnic.setText(idCardObjUser.getIdentCardEthnic());
            }
        }
    }

    private void mapping() {
        txtCardType = findViewById(R.id.txt_card_type);
        txtIdCard = findViewById(R.id.txt_id_card);
        txtFullName = findViewById(R.id.txt_fullname);
        txtDateOfBirth = findViewById(R.id.txt_date_of_birth);
        txtHometown = findViewById(R.id.txt_hometown);//que quan
        txtAddressResidence = findViewById(R.id.txt_address_residence);// dia chi thuong tru
        txtGender = findViewById(R.id.txt_gender);
        txtIdentCardIssueDate = findViewById(R.id.txt_ident_card_issue_date);//ngay cap
        txtExpireDate = findViewById(R.id.txt_expire_date);//co gia tri den
        txtNation_ethnic = findViewById(R.id.txt_nation_ethnic);//quoc tich/ dan toc
        imgFrontCard = findViewById(R.id.img_card_front);
        imgBackCard = findViewById(R.id.img_card_back);
        btnXacNhanCmt = findViewById(R.id.btn_xac_nhan_cmt);
        txtLiveness = findViewById(R.id.txt_liveness);
        imgFace = findViewById(R.id.img_face);
        viewInfor = findViewById(R.id.view_infor);
    }

    private void getFilePathCardFromOCR() {
        //card
        SharedPreferences sharedPreferences = getSharedPreferences("fileCard", Context.MODE_PRIVATE);
        //face
        SharedPreferences sharedPreferencesFace = getSharedPreferences("fileImageFace", Context.MODE_PRIVATE);
        if (sharedPreferences != null && sharedPreferencesFace != null) {
            String fileFrontCard = sharedPreferences.getString("fileFrontCard", "");
            String fileBackCard = sharedPreferences.getString("fileBackCard", "");
            String fileFace1 = sharedPreferencesFace.getString("nameface1", "");
            String fileFace2 = sharedPreferencesFace.getString("nameface2", "");
            String fileFace3 = sharedPreferencesFace.getString("nameface3", "");
            Log.d("fileFrontCard", fileFrontCard);
            Log.d("fileBackCard", fileBackCard);
            Log.d("fileFace1", fileFace1);
            File fileFront = new File(fileFrontCard);
            File fileBack = new File(fileBackCard);
            File fileFace11 = new File(fileFace1);
            if (fileFront.exists() && fileBack.exists() && fileFace11.exists()) {
                imgFace.setVisibility(View.VISIBLE);
                Glide.with(getApplicationContext()).load(fileFront).into(imgFrontCard);
                Glide.with(getApplicationContext()).load(fileBack).into(imgBackCard);
                Glide.with(getApplicationContext()).load(fileFace11).into(imgFace);
            } else {
                Toast.makeText(getApplicationContext(), "File not Exist", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_xac_nhan_cmt:// sau khi nhan dien khuon mat
                if (rootLivess.getCode() == 200 || rootLivess.getCode() == 500 || rootLivess.getCode() == 501 || rootLivess.getCode() == 502) {
                    Log.d("isMatchingResult1", dataLivess.isMatchingResult() + "");
                    Intent intent = new Intent(ResultActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Log.d("isMatchingResult2", dataLivess.isMatchingResult() + "");
                    Intent returnIntent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("result", idCardObjUser);
                    returnIntent.putExtras(bundle);
                    setResult(RESULT_CODE_TAKE_FACE_AGAIN, returnIntent);
                    Log.d("userinfor3", idCardObjUser.toString());
                    finish();
                }
                break;
        }
    }

}