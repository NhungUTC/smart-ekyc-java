package com.hyperlogy.smartekyc.EkycUntil.EkycApi;

import com.hyperlogy.smartekyc.EkycModel.ItemFormData;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class Api {
private static final String PUBLIC_KEY = "12GdkY$bg^%3kju5!";
//    private static String BASE_JWT = "http://14.224.132.206:8682";
//    private static String BASE_API = "http://14.224.132.206:8681";

    private static String BASE_JWT = "https://jwtapibeta.hyperlogy.com";
    private static String BASE_API = "https://bioapibeta.hyperlogy.com";
    //TODO
    public static String getUrlGetToken() {
        return BASE_JWT + "/api/token/Get?publicKey=" + PUBLIC_KEY;
    }

    public static String getUrlOCR() {
        return BASE_API + "/api-v2/process-automation/OCR";
    }

    public static String getUrlVerifyFace() {
        return BASE_API + "/api/FaceVerify/Liveness";
    }

    public static RequestBody getRequestBodyVerify(List<Object> objectList) {
        //3 anh khuon mat+ guiId
        ItemFormData itemFormData1 = (ItemFormData) objectList.get(0);
        ItemFormData itemFormData2 = (ItemFormData) objectList.get(1);
        ItemFormData itemFormData3 = (ItemFormData) objectList.get(2);
        String guiId = (String) objectList.get(3);
        String userName = (String) objectList.get(4);
        //check dang nhap, dang ky
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("CaptureImage1", itemFormData1.getName(), RequestBody.create(MediaType.parse("image/jpeg"), itemFormData1.getmFile()))
                .addFormDataPart("CaptureImage2", itemFormData2.getName(), RequestBody.create(MediaType.parse("image/jpeg"), itemFormData2.getmFile()))
                .addFormDataPart("CaptureImage3", itemFormData3.getName(), RequestBody.create(MediaType.parse("image/jpeg"), itemFormData3.getmFile()))
                .addFormDataPart("GuidID", guiId)
                .addFormDataPart("Username", userName)
                .build();
        return requestBody;
    }

    public static RequestBody getRequestBodyOCR(List<Object> objectList) {
        //ảnh mặt trước, mặt sau cmt,
        ItemFormData imageFront = (ItemFormData) objectList.get(0);
        ItemFormData imageBack = (ItemFormData) objectList.get(1);
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("IdCardFront", imageFront.getName(), RequestBody.create(MediaType.parse("image/jpeg"), imageFront.getmFile()))
                .addFormDataPart("IdCardBack", imageBack.getName(), RequestBody.create(MediaType.parse("image/jpeg"), imageBack.getmFile()))
                .build();
        return requestBody;
    }
}
