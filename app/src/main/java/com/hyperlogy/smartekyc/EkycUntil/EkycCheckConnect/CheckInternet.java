package com.hyperlogy.smartekyc.EkycUntil.EkycCheckConnect;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.hyperlogy.smartekyc.EkycActivity.LoginActivity;

public class CheckInternet {
    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
    public  static void showToastLong(Context context, String tb ){
        Toast.makeText(context,""+"Bạn hãy kiểm tra lại kết nối Internet", Toast.LENGTH_SHORT).show();
    }

    public static void dialogGetToken(Context context, String title, String message, String textPBtn){
        AlertDialog.Builder b= new AlertDialog.Builder(context);
        b.setCancelable(false);
        b.setTitle(title);
        b.setMessage(message);
        b.setPositiveButton(textPBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        b.show();
    }
    public  static void checkTimeoutPost(Context context,String title, String message, String textBtn){
        AlertDialog.Builder b= new AlertDialog.Builder(context);
        b.setCancelable(false);
        b.setTitle(title);
        b.setMessage(message);
        b.setPositiveButton(textBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent= new Intent(context, LoginActivity.class);
                context.startActivity(intent);
            }
        });
        b.show();
    }
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

}
