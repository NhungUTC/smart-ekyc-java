package com.hyperlogy.smartekyc.EkycModel;


import java.io.Serializable;

public class ResultFace  {
    public static class DataLivess implements Serializable {

        private boolean livenessResult;
        private boolean matchingResult;
        private double matchingRate;
        private int imageNumber;

        public DataLivess() {
        }

        public DataLivess(boolean livenessResult, boolean matchingResult, double matchingRate, int imageNumber) {
            this.livenessResult = livenessResult;
            this.matchingResult = matchingResult;
            this.matchingRate = matchingRate;
            this.imageNumber = imageNumber;
        }

        public boolean isLivenessResult() {
            return livenessResult;
        }

        public void setLivenessResult(boolean livenessResult) {
            this.livenessResult = livenessResult;
        }

        public boolean isMatchingResult() {
            return matchingResult;
        }

        public void setMatchingResult(boolean matchingResult) {
            this.matchingResult = matchingResult;
        }

        public double getMatchingRate() {
            return matchingRate;
        }

        public void setMatchingRate(double matchingRate) {
            this.matchingRate = matchingRate;
        }

        public int getImageNumber() {
            return imageNumber;
        }

        public void setImageNumber(int imageNumber) {
            this.imageNumber = imageNumber;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "livenessResult=" + livenessResult +
                    ", matchingResult=" + matchingResult +
                    ", matchingRate=" + matchingRate +
                    ", imageNumber=" + imageNumber +
                    '}';
        }

    }

    public static class RootLivess implements Serializable{
        private String messages;
        private int code;
        private boolean success;
        private DataLivess data;

        public RootLivess() {
        }

        public RootLivess(String messages, int code, boolean success, DataLivess data) {
            this.messages = messages;
            this.code = code;
            this.success = success;
            this.data = data;
        }

        public String getMessages() {
            return messages;
        }

        public void setMessages(String messages) {
            this.messages = messages;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public DataLivess getData() {
            return data;
        }

        public void setData(DataLivess data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return "Root{" +
                    "messages='" + messages + '\'' +
                    ", code=" + code +
                    ", success=" + success +
                    ", data=" + data +
                    '}';
        }


    }
}
