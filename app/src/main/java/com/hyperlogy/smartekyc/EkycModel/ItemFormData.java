package com.hyperlogy.smartekyc.EkycModel;

import java.io.File;

public class ItemFormData {
    private File mFile;
    private String name;
    private String type;
    public ItemFormData(File mFile, String name, String type) {
        this.mFile = mFile;
        this.name = name;
        this.type = type;
    }

    public File getmFile() {
        return mFile;
    }

    public void setmFile(File mFile) {
        this.mFile = mFile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return
                "mFile=" + mFile +
                ", name='" + name + '\'' +
                ", type='" + type + '\'';
    }
}
