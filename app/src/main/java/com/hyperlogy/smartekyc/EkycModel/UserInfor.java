package com.hyperlogy.smartekyc.EkycModel;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class UserInfor {
    public UserInfor() {
    }

    public static class IdCard implements Serializable {
        private String identCardType;//
        private String identCardNumber;//
        private String identCardName;//
        private String identCardNameOther;
        private String identCardBirthDate;//
        private String identCardGender;//
        private String identCardNation;//
        private String identCardEthnic;
        private String identCardCountry;//
        private String identCardAdrResidence;//
        private String identCardExpireDate;//
        private String identCardIssueDate;//
        private String identCardIssuePlace;
        private String identCardRaw;
        private String errorDataFront;
        private String outputDataFront;
        private String msgDataFront;
        private String errorDataBack;
        private String outputDataBack;
        private String msgDataBack;

        public IdCard() {
        }

        public IdCard(String identCardType, String identCardNumber, String identCardName, String identCardBirthDate, String identCardGender, String identCardNation, String identCardCountry, String identCardAdrResidence, String identCardExpireDate, String identCardIssueDate) {
            this.identCardType = identCardType;
            this.identCardNumber = identCardNumber;
            this.identCardName = identCardName;
            this.identCardBirthDate = identCardBirthDate;
            this.identCardGender = identCardGender;
            this.identCardNation = identCardNation;
            this.identCardCountry = identCardCountry;
            this.identCardAdrResidence = identCardAdrResidence;
            this.identCardExpireDate = identCardExpireDate;
            this.identCardIssueDate = identCardIssueDate;
        }

        public String getIdentCardType() {
            return identCardType;
        }

        public void setIdentCardType(String identCardType) {
            this.identCardType = identCardType;
        }

        public String getIdentCardNumber() {
            return identCardNumber;
        }

        public void setIdentCardNumber(String identCardNumber) {
            this.identCardNumber = identCardNumber;
        }

        public String getIdentCardName() {
            return identCardName;
        }

        public void setIdentCardName(String identCardName) {
            this.identCardName = identCardName;
        }

        public String getIdentCardBirthDate() {
            return identCardBirthDate;
        }

        public void setIdentCardBirthDate(String identCardBirthDate) {
            this.identCardBirthDate = identCardBirthDate;
        }

        public String getIdentCardGender() {
            return identCardGender;
        }

        public void setIdentCardGender(String identCardGender) {
            this.identCardGender = identCardGender;
        }

        public String getIdentCardNation() {
            return identCardNation;
        }

        public void setIdentCardNation(String identCardNation) {
            this.identCardNation = identCardNation;
        }

        public String getIdentCardCountry() {
            return identCardCountry;
        }

        public void setIdentCardCountry(String identCardCountry) {
            this.identCardCountry = identCardCountry;
        }

        public String getIdentCardAdrResidence() {
            return identCardAdrResidence;
        }

        public void setIdentCardAdrResidence(String identCardAdrResidence) {
            this.identCardAdrResidence = identCardAdrResidence;
        }

        public String getIdentCardExpireDate() {
            return identCardExpireDate;
        }

        public void setIdentCardExpireDate(String identCardExpireDate) {
            this.identCardExpireDate = identCardExpireDate;
        }

        public String getIdentCardIssueDate() {
            return identCardIssueDate;
        }

        public void setIdentCardIssueDate(String identCardIssueDate) {
            this.identCardIssueDate = identCardIssueDate;
        }

        public String getIdentCardEthnic() {
            return identCardEthnic;
        }

        public void setIdentCardEthnic(String identCardEthnic) {
            this.identCardEthnic = identCardEthnic;
        }

        @Override
        public String toString() {
            return "IdCard{" +
                    "identCardType='" + identCardType + '\'' +
                    ", identCardNumber='" + identCardNumber + '\'' +
                    ", identCardName='" + identCardName + '\'' +
                    ", identCardBirthDate='" + identCardBirthDate + '\'' +
                    ", identCardGender='" + identCardGender + '\'' +
                    ", identCardNation='" + identCardNation + '\'' +
                    ", identCardCountry='" + identCardCountry + '\'' +
                    ", identCardAdrResidence='" + identCardAdrResidence + '\'' +
                    ", identCardExpireDate='" + identCardExpireDate + '\'' +
                    ", identCardIssueDate='" + identCardIssueDate + '\'' +
                    ", identCardEthnic='" + identCardEthnic + '\'' +
                    '}';
        }
    }
//--------------------------------------------------------------------------------------------------

    public class Data{
        private IdCard idCard;
        private Object rsCus;
        private String guidID;
        private Object urlXml;
        private Object cif;

        public Data(IdCard idCard, Object rsCus, String guidID, Object urlXml, Object cif) {
            this.idCard = idCard;
            this.rsCus = rsCus;
            this.guidID = guidID;
            this.urlXml = urlXml;
            this.cif = cif;
        }

        public String getGuidID() {
            return guidID;
        }

        public void setGuidID(String guidID) {
            this.guidID = guidID;
        }

        public IdCard getIdCard() {
            return idCard;
        }

        public void setIdCard(IdCard idCard) {
            this.idCard = idCard;
        }

        public Object getRsCus() {
            return rsCus;
        }

        public void setRsCus(Object rsCus) {
            this.rsCus = rsCus;
        }

        public Object getUrlXml() {
            return urlXml;
        }

        public void setUrlXml(Object urlXml) {
            this.urlXml = urlXml;
        }

        public Object getCif() {
            return cif;
        }

        public void setCif(Object cif) {
            this.cif = cif;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "idCard=" + idCard +
                    ", rsCus=" + rsCus +
                    ", guidID='" + guidID + '\'' +
                    ", urlXml=" + urlXml +
                    ", cif=" + cif +
                    '}';
        }
    }
//----------------------------------------------------------------------------------
    public static class Root implements Serializable, Parcelable {
        private String messages;
        private int code;
        private boolean success;
        private Data data;

    public Root() {
    }

    public Root(String messages, int code, boolean success, Data data) {
            this.messages = messages;
            this.code = code;
            this.success = success;
            this.data = data;
        }

    protected Root(Parcel in) {
        messages = in.readString();
        code = in.readInt();
        success = in.readByte() != 0;
    }

    public static final Creator<Root> CREATOR = new Creator<Root>() {
        @Override
        public Root createFromParcel(Parcel in) {
            return new Root(in);
        }

        @Override
        public Root[] newArray(int size) {
            return new Root[size];
        }
    };

    public String getMessages() {
            return messages;
        }

        public void setMessages(String messages) {
            this.messages = messages;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    @Override
    public String toString() {
        return "Root{" +
                "messages='" + messages + '\'' +
                ", code=" + code +
                ", success=" + success +
                ", data=" + data +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(messages);
        dest.writeInt(code);
        dest.writeByte((byte) (success ? 1 : 0));
    }
}
}
